from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort
import json
import uuid
import sqlite3
import requests
import datetime
from views import *
from mangopay_globals import app, msg91_key

''' Filters '''

@app.template_filter('datetimeformat')
def datetimeformat(value, format):
	d_obj = datetime.datetime.strptime(value, '%Y-%m-%d')
	return d_obj.strftime(format)

@app.template_filter('timestampformat')
def timestampformat(value, format):
	d_obj = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
	current_date = datetime.date.today()
	timestamp_date = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S').date()
	if timestamp_date == current_date:
		timestamp_time = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S').time()
		return "Today at {}".format(timestamp_time.strftime('%I:%M %p'))
	else:
		return d_obj.strftime(format)

''' Blueprints '''
# Administration
app.register_blueprint(admin_access)
app.register_blueprint(admin_dashboard)

# Registration and Payment
app.register_blueprint(index)
app.register_blueprint(registration)
app.register_blueprint(payment)
app.register_blueprint(confirmation)

# Course - Index, create, and edit
app.register_blueprint(course_index)

# Registration - Index and single
app.register_blueprint(registration_index)

# Location - Index and create
app.register_blueprint(location_index)

# Events - Index, create, edit, and delete
app.register_blueprint(event_index)

# API functions for AJAX calls
app.register_blueprint(api_farm)

# OAuth
app.register_blueprint(oauth_index)

# Contact
app.register_blueprint(contact_index)

''' Error Pages '''
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal(error):
	return render_template('500.html'), 500

''' App 
'''
if __name__ == '__main__':
    app.run()