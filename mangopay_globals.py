from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort
import sqlite3
import razorpay
import requests
from flask_mail import Mail, Message
from tasks import make_celery, configure_flask_integration
from celery import Celery

app = Flask(__name__, static_folder="static", static_url_path='/static')

''' Configs '''
#config keys
''' Email '''
def send_mail(recipient, parent_name, course_name, message):
	try:
		msg = Message("Registration",
					sender="support@example.co",
					recipients=[recipient],
					bcc=['example@gmail.com'])
		msg.body = message
		mail.send(msg)
	except SMTPException:
		pass

''' SMS '''
def send_sms(recipient, parent_name, course_name, message):
	payload = {
	  "sender": "abcdef",
	  "route": "4",
	  "country": "91",
	  "sms": [
	    {
	      "message": message,
	      "to": [
	        recipient
	      ]
	    }
	  ]
	}
	headers = {
	    'authkey': msg91_key,
	    'content-type': "application/json"
	    }

	url = "http://api.msg91.com/api/v2/sendsms"
	requests.post(url, json=payload, headers=headers)

''' App Context Database Operations 
'''

# Database connection
def connect_db():
	c = sqlite3.connect(app.config['DATABASE'])
	c.execute("PRAGMA foreign_keys=ON")
	return c

# Initialize database
def init_db():
	db = get_db()
	with app.open_resource('schema.sql', mode='r') as f:
		db.cursor().executescript(f.read())
	db.commit()

# Alter database
def alter_db():
    db = get_db()
    with app.open_resource('alter_schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
	init_db()
	print('Database initialised.')

@app.cli.command('alterdb')
def alterdb_command():
	init_db()
	print('Database schema updated.')

# Opens a new database connection for the current application context
def get_db():
	if not hasattr(g, 'sqlite_db'):
		g.sqlite_db = connect_db()
		g.sqlite_db.row_factory = sqlite3.Row
	return g.sqlite_db

# Closes the database again at the end of the request
@app.teardown_appcontext
def close_db(error):
	if hasattr(g, 'sqlite_db'):
		g.sqlite_db.close()

''' Task '''
celery_app = Celery(
	app.import_name,
	broker=app.config['CELERY_BROKER_URL'],
	autofinalize=False
	)
#celery = make_celery(app)
celery = configure_flask_integration(app, celery_app)