CREATE TABLE IF NOT EXISTS  tmp_course (
  course_id INTEGER PRIMARY KEY AUTOINCREMENT,
  course_uid STRING UNIQUE,
  title STRING,
  online_fees INTEGER,
  offline_fees INTEGER,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  camp_total_online_fees INTEGER
);
CREATE TABLE IF NOT EXISTS  tmp_parent_registration (
  parent_id INTEGER PRIMARY KEY AUTOINCREMENT,
  parent_uid STRING UNIQUE,
  parent_name STRING,
  parent_email STRING,
  parent_contact INTEGER,
  kid_name STRING,
  course_id INTEGER,
  mode_of_payment STRING,
  online_payment_status STRING,
  payment_id STRING,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  event_id INTEGER,
  UNIQUE (parent_email, kid_name, event_id)
  FOREIGN KEY(course_id) REFERENCES tmp_course(course_id)
  ON UPDATE CASCADE ON DELETE CASCADE
  FOREIGN KEY(event_id) REFERENCES tmp_event(event_id)
  ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS tmp_location (
  location_id INTEGER PRIMARY KEY AUTOINCREMENT,
  location STRING,
  UNIQUE (location)
);
CREATE TABLE IF NOT EXISTS tmp_event (
  event_id INTEGER PRIMARY KEY AUTOINCREMENT,
  location_id INTEGER,
  course_id INTEGER,
  date STRING,
  UNIQUE (location_id, course_id, date)
  FOREIGN KEY(location_id) REFERENCES tmp_location(location_id)
  ON UPDATE CASCADE ON DELETE CASCADE
  FOREIGN KEY(course_id) REFERENCES tmp_course(course_id)
  ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS tmp_oauth (
  zoho_oauth_id INTEGER PRIMARY KEY AUTOINCREMENT,
  zoho_refresh_token STRING,
  zoho_auth_token STRING
);