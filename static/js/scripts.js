// Disable the register button until the checkbox is clicked

$(function() {
  var checkbox = $('#agreement');
  var button = $('#mango-register');

  checkbox.on('change', function() {
    button.prop("disabled", !this.checked);//true: disabled, false: enabled
  }).trigger('change'); //page load trigger event
});