from celery import Celery

def make_celery(app):
	celery = Celery(
		app.import_name,
		broker=app.config['CELERY_BROKER_URL']
		)
	celery.conf.update(app.config)
	class ContextTask(celery.Task):
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return self.run(*args, **kwargs)

	celery.Task = ContextTask
	return celery

# https://www.reddit.com/r/flask/comments/8tyluz/celery_task_in_flask_running_out_of_app_context/
def configure_flask_integration(app, celery):
    celery.conf.update(app.config)

    TaskBase = celery.Task

    class AppContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = AppContextTask

    celery.finalize()

    return celery