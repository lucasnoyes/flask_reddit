from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
from mangopay_globals import app

''' Login '''

admin_access = Blueprint('admin_access', __name__)

@admin_access.route('/login', methods=['GET', 'POST'])
def login():
	error = None
	if request.method == 'POST':
		if request.form['username'] != app.config['USERNAME']:
			flash("Invalid credentials", "error")
		elif request.form['password'] != app.config['PASSWORD']:
			flash("Invalid credentials", "error")
		else:
			session['username'] = request.form['username']
			flash('Login successful! Duh!', "success")
			return redirect(url_for('dashboard.dashboard'))
	return render_template('login.html')

''' Logout '''
@admin_access.route('/logout')
def logout():
	session.pop('username', None)
	flash('You are logged out!', "success")
	return redirect(url_for('admin_access.login'))