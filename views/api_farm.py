from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import requests
from mangopay_globals import msg91_key

''' Administration '''

api_farm = Blueprint('api_farm', __name__)

@api_farm.route('/api_farm/xkcd', methods=['GET'])
def xkcd():
	if 'username' in session:
		# XKCD
		try:
			xkcd_url = "https://xkcd.com/info.0.json"
			xkcd2 = requests.get(xkcd_url).json()
		except:
			xkcd2 = None
		return xkcd2
	else:
		return render_template('login.html')