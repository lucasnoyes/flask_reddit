from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
import razorpay
from mangopay_globals import celery, app
import contact_index

''' Confirmations and API calls '''

confirmation = Blueprint('confirmation', __name__)

@confirmation.route('/confirmation/<course_uid>', methods=['GET'])
def confirm(course_uid):
    name = session['parent_name']
    email = session['parent_email']
    number = session['parent_contact']
    contact_index.send_contact.delay(name, email, number)
    return render_template('confirmation.html', course_uid=course_uid)