from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import requests
import datetime
import json
from mangopay_globals import zoho_books_client_id, zoho_books_client_secret, zoho_organization_id, celery, app
import oauth
''' Contacts Zoho '''

contact_index = Blueprint('contact_index', __name__)

@contact_index.route('/contacts', methods=['GET', 'POST'])
def view_contacts():
	if request.method == "GET":
		zoho_auth_token = oauth.get_zoho_auth_token()
		if zoho_auth_token:
			contacts_response = requests.get("https://books.zoho.com/api/v3/contacts",
				params = {
				'organization_id': zoho_organization_id
				},
				headers = {
				'Authorization': "Zoho-oauthtoken {}".format(zoho_auth_token),
				'Content-Type': "application/x-www-form-urlencoded;charset=UTF-8"
				})
			items_response = requests.get("https://books.zoho.com/api/v3/items",
				params = {
				'organization_id': zoho_organization_id
				},
				headers = {
				'Authorization': "Zoho-oauthtoken {}".format(zoho_auth_token),
				'Content-Type': "application/x-www-form-urlencoded;charset=UTF-8"
				})

			if contacts_response.status_code == 400:
				oauth.refresh_zoho_auth_token()
				view_contacts()
			if contacts_response.status_code == 500:
				flash("Internal API error", 'error')
			contacts = json.loads(contacts_response.text)
			items = json.loads(items_response.text)

			return render_template('contact-index.html', contacts=contacts, items=items)
		else:
			return render_template('contact-index.html')

@celery.task
def send_contact(name, email, number):
	# Check Contact
	# Add Contact
	zoho_auth_token = oauth.get_zoho_auth_token()
	if zoho_auth_token:
		var = {
				"contact_name": name,
				"billing_address": {
				    "phone": number
				},
				"contact_persons": [
					{
						"email": email
					}
				]
			}

		response = requests.post("https://books.zoho.com/api/v3/contacts",
			params = {
			'organization_id': zoho_organization_id,
			'JSONString': json.dumps(var)
			},
			headers = {
			'Authorization': "Zoho-oauthtoken {}".format(zoho_auth_token),
			'Content-Type': "application/x-www-form-urlencoded;charset=UTF-8"
			})
		print response.text
		if response.status_code == 400:
			oauth.refresh_zoho_auth_token()
			print response.text
		if response.status_code == 500:
			flash("Internal API error", 'error')