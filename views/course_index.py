from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db

''' Course - Index, create, and edit '''

course_index = Blueprint('course_index', __name__)

''' Indexing and Creation '''
@course_index.route('/courses', methods=['GET', 'POST'])
def courses():
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			uid = uuid.uuid4()

			''' The following query shows all courses that has 
				events or locations associated to it.
			''' 
			cur_courses = db.execute('SELECT tmp_course.*, \
			tmp_location.location, tmp_event.date FROM tmp_event \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_event.course_id \
			INNER JOIN tmp_location ON tmp_event.location_id = tmp_location.location_id \
			GROUP BY tmp_course.course_uid')
			courses = cur_courses.fetchall()
			cur_locations = db.execute('SELECT * FROM tmp_location')
			locations = cur_locations.fetchall()

			''' The following query shows the rest of the courses
				that are orphaned without any events or locations.
			'''
			cur_orphaned_courses = db.execute('SELECT tmp_course.* FROM tmp_course \
			WHERE NOT EXISTS (SELECT tmp_event.course_id FROM tmp_event \
			WHERE tmp_course.course_id = tmp_event.course_id)')
			orphaned_courses = cur_orphaned_courses.fetchall()
			return render_template('course-index.html', courses=courses, 
				orphaned_courses=orphaned_courses, locations=locations, uid=uid)
		elif request.method == 'POST':
			course_uid = request.form['course_uid']
			title = request.form['course_name']
			online_fees = request.form['online_fees']
			offline_fees = request.form['offline_fees']
			locations = request.form.getlist('location')
			db.execute('INSERT INTO tmp_course (course_uid, title, online_fees, offline_fees) \
				VALUES (?, ?, ?, ?)', [course_uid, title, online_fees, offline_fees])
			cur_course_id = db.execute('SELECT course_id FROM tmp_course WHERE course_uid=?', [course_uid])
			course_id = cur_course_id.fetchone()[0]
			for location_id in locations:
				db.execute('INSERT INTO tmp_event (course_id, location_id) \
					VALUES (?, ?)', [course_id, location_id])
			db.commit()
			flash("Course successfully created. Please add a new event to the course below.", "success info")
			return redirect(url_for('event_index.events', course_id=course_id))
	else:
		return render_template('login.html')

''' Edit '''
@course_index.route('/course/<uid>/edit', methods=['GET', 'POST'])
def edit_course(uid):
	if 'username' in session:
		db = get_db()
		cur_course_id = db.execute('SELECT course_id FROM tmp_course WHERE course_uid=?', [uid])
		course_id = cur_course_id.fetchone()[0]
		if request.method == 'GET':
			cur_course = db.execute('SELECT tmp_course.*, \
			tmp_location.location FROM tmp_event \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_event.course_id \
			INNER JOIN tmp_location ON tmp_event.location_id = tmp_location.location_id \
			WHERE tmp_course.course_uid = ?', [uid])
			course = cur_course.fetchall()[0]
			return render_template('course-edit.html', course=course)

		elif request.method == 'POST':
			title = request.form['course_name']
			online_fees = request.form['online_fees']
			offline_fees = request.form['offline_fees']
			camp_total_online_fees = request.form['camp_total_online_fees']
			db.execute('UPDATE tmp_course SET title = ?, online_fees = ?, \
				offline_fees = ?, camp_total_online_fees = ? \
				WHERE course_uid = ?', [title, online_fees, offline_fees, camp_total_online_fees, uid])
			db.commit()
			flash("Successfully edited the course.", "success")
			return redirect(url_for('course_index.edit_course', uid=uid))
	else:
		return render_template('login.html')

@course_index.route('/orphan-course/<uid>/edit', methods=['GET', 'POST'])
def edit_course_orphan(uid):
	if 'username' in session:
		db = get_db()
		cur_course_id = db.execute('SELECT course_id FROM tmp_course WHERE course_uid=?', [uid])
		course_id = cur_course_id.fetchone()[0]
		if request.method == 'GET':
			cur_course = db.execute('SELECT * FROM tmp_course \
			WHERE course_uid = ?', [uid])
			course = cur_course.fetchone()
			return render_template('course-edit-orphan.html', course=course)

		elif request.method == 'POST':
			title = request.form['course_name']
			online_fees = request.form['online_fees']
			offline_fees = request.form['offline_fees']
			camp_total_online_fees = request.form['camp_total_online_fees']
			db.execute('UPDATE tmp_course SET title = ?, online_fees = ?, \
				offline_fees = ?, camp_total_online_fees = ? \
				WHERE course_uid = ?', [title, online_fees, offline_fees, camp_total_online_fees, uid])
			db.commit()
			flash("Successfully edited the course.", "success")
			return redirect(url_for('course_index.edit_course_orphan', uid=uid))
	else:
		return render_template('login.html')

''' Registration Link '''
@course_index.route('/<course_name>/<course_uid>/registration-link', methods=['GET'])
def generate_registration_link(course_name, course_uid):
	if 'username' in session:
		registration_link = '<a class="registration button" id="mango_registration_payment" target="_blank" href="{}?course_uid={}">Register</a>'.format(request.url_root, course_uid)
		just_the_registration_link = '{}?course_uid={}'.format(request.url_root, course_uid)
		return render_template('registration-link.html', registration_link=registration_link, 
			just_the_registration_link=just_the_registration_link, course_name=course_name)
	else:
		return render_template('login.html')