from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import requests
import sqlite3
from mangopay_globals import msg91_key, get_db

''' Administration '''

admin_dashboard = Blueprint('dashboard', __name__)

@admin_dashboard.route('/dashboard', methods=['GET'])
def dashboard():
	if 'username' in session:
		''' Counters '''
		counters = {}

		db = get_db()
		counters['course'] = db.execute('SELECT Count(*) FROM tmp_course').fetchone()[0]
		counters['event'] = db.execute('SELECT Count(*) FROM tmp_event').fetchone()[0]
		counters['registration'] = db.execute('SELECT Count(*) FROM tmp_parent_registration').fetchone()[0]
		counters['location'] = db.execute('SELECT Count(*) FROM tmp_location').fetchone()[0]

		''' APIs '''
		# MSG91
		try:
			msg91_url = "https://control.msg91.com/api/balance.php?authkey={}&type=1".format(msg91_key)
			message_balance = requests.get(msg91_url).text
		except:
			message_balance = "API Error"

		# XKCD
		try:
			xkcd_url = "https://xkcd.com/info.0.json"
			xkcd = requests.get(xkcd_url).json()
		except:
			xkcd = None

		# Design Quotes
		try:
			quotes_url = "https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1"
			quote = requests.get(quotes_url).json()
			print quote
		except:
			quote = None 

		return render_template('dashboard.html', message_balance=message_balance, xkcd=xkcd, 
			themango_posts=themango_posts, quote=quote, counters=counters)
	else:
		return render_template('login.html')