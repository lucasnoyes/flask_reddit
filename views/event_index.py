from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db

event_index = Blueprint('event_index', __name__)

@event_index.route('/<course_id>/event', methods=['GET', 'POST'])
def events(course_id):
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_locations = db.execute('SELECT location_id, location FROM tmp_location')
			locations = cur_locations.fetchall()
			cur_events = db.execute('SELECT tmp_event.event_id, tmp_course.title, \
			tmp_location.location, tmp_event.date FROM tmp_event \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_event.course_id \
			INNER JOIN tmp_location ON tmp_location.location_id = tmp_event.location_id \
			WHERE tmp_event.course_id = ?', [course_id])
			events = cur_events.fetchall()
			cur_course_info = db.execute('SELECT course_uid, title FROM tmp_course WHERE \
			course_id=?', [course_id])
			for course_uid, course_title in cur_course_info.fetchall():
				course_uid = course_uid
				course_title = course_title
			return render_template('event-index.html', events=events,
					course_id=course_id, locations=locations, 
					course_title=course_title, course_uid=course_uid)
		elif request.method == 'POST':
			date = request.form['date']
			location_id = request.form['location']
			try:
				db.execute('INSERT INTO tmp_event (location_id, course_id, date) \
					VALUES (?, ?, ?)', [location_id, course_id, date])
				db.commit()
				flash("Event successfully added!", "success")
			except sqlite3.IntegrityError as e:
				flash("Event exists already!", "error")
			return redirect(url_for('event_index.events', course_id=course_id))
	else:
		return render_template('login.html')
@event_index.route('/event/<event_id>/edit', methods=['GET', 'POST'])
def edit_event(event_id):
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_events = db.execute('SELECT tmp_event.event_id, tmp_course.course_id, \
			tmp_course.title, tmp_location.location, tmp_event.date \
			FROM tmp_event INNER JOIN tmp_course ON tmp_course.course_id = tmp_event.course_id \
			INNER JOIN tmp_location ON tmp_location.location_id = tmp_event.location_id \
			WHERE tmp_event.event_id = ?', [event_id])
			event = cur_events.fetchone()
			cur_locations = db.execute('SELECT * FROM tmp_location')
			locations = cur_locations.fetchall()
			return render_template('event-edit.html', event=event, locations=locations)
		elif request.method == 'POST':
			try:
				date = request.form['date']
				location_id = request.form['location']
				course_id = request.form['course_id']
				db.execute('UPDATE tmp_event SET location_id = ?, date = ? \
				WHERE event_id = ?', [location_id, date, event_id])
				db.commit()
				flash('Event updated!', "success")
			except sqlite3.IntegrityError:
				flash('Event already exists!', "error")
			return redirect(url_for('event_index.events', course_id=course_id))
	else:
		return render_template('login.html')

@event_index.route('/event/<event_id>/confirm-delete', methods=['GET'])
def confirm_delete_event(event_id):
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_course_id = db.execute('SELECT course_id FROM tmp_event \
			WHERE event_id=?',[event_id])
			course_id = cur_course_id.fetchone()[0]
			flash('Are you sure you want to delete? This will \
			also affect the previous registrations associated with this event. \
			It is recommended to ignore this event for archival purposes. \
			If you had just created the event and no registrations have been done, \
			you may delete it.', "warning")
			return render_template('event-delete-confirm.html', event_id=event_id, 
				course_id=course_id)
	else:
		return render_template('login.html')

@event_index.route('/event/<event_id>/delete', methods=['POST'])
def delete_event(event_id):
	if 'username' in session:
		db = get_db()
		if request.method == 'POST':
			course_id = request.form['course_id']
			db.execute('DELETE FROM tmp_event WHERE event_id=?', [event_id])
			flash('Event deleted successfully.', "success")
			db.commit()
			return redirect(url_for('event_index.events', course_id=course_id))
	else:
		return render_template('login.html')