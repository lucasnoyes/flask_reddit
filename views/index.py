from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db

''' Index '''

index = Blueprint('index', __name__)

@index.route('/')
def app_create():
	if request.args.get('course_uid'):
		course_uid = request.args.get('course_uid')
		parent_uid = uuid.uuid4()
		db = get_db()
		cur_payment = db.execute('SELECT course_uid, title, online_fees, \
			offline_fees, course_id, camp_total_online_fees \
			FROM tmp_course WHERE course_uid=?', [course_uid])
		try:
			course = cur_payment.fetchone()
			if isinstance(course[5], (int, long)) or (course[5] is None):
				cur_locations = db.execute('SELECT tmp_event.event_id,  \
				tmp_location.location, tmp_event.date FROM tmp_location \
				INNER JOIN tmp_event ON tmp_event.location_id = tmp_location.location_id \
				WHERE tmp_event.course_id = ?', [course[4]])
				all_locations = cur_locations.fetchall()
				valid_locations = {}
				''' This loops over the location data to do a date comparison
					to automatically remove the locations where events were expired.
					This also closes the registrations automatically.
				'''
				for location in all_locations:
					course_date = datetime.datetime.strptime(location[2], '%Y-%m-%d').date()
					current_date = datetime.date.today()
					if current_date >= course_date:
						valid_locations[location] = 'expired'
					else:
						valid_locations[location] = 'in progress'
			else:
				flash("TOF default not set.", "error_text")
				abort(500)

		except (IndexError, TypeError) as e:
			flash("This is not a valid course ID.\
				Sorry for the missing link. It's out there somewhere! \
				We'll get it soon. Try again later. Okay? Fine? Cool! \
				Now, if you are an admin, please locate or create the course. \
				If the course exists, please associate an event to it.", "warning")
			abort(404)
			course = None
			valid_locations = None
		return render_template('app.html', course=course, locations=valid_locations, 
			parent_uid=parent_uid)
		
	else:
		abort(404)