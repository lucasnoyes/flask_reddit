from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db

location_index = Blueprint('location_index', __name__)

''' Location index '''
@location_index.route('/locations', methods=['GET', 'POST'])

def locations():
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_locations = db.execute('SELECT location_id, location FROM tmp_location')
			locations = cur_locations.fetchall()
			return render_template('course-locations.html', locations=locations)
		elif request.method == 'POST':
			try:
				location = request.form['course-location']
				db.execute('INSERT INTO tmp_location (location) VALUES (?)', [location])
				db.commit()
			except sqlite3.IntegrityError:
				flash('Location already exists!',"error")
			return redirect(url_for('locations'))
	else:
		return render_template('login.html')