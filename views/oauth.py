from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint, make_response
import requests
import datetime
import sqlite3
from mangopay_globals import zoho_books_client_id, zoho_books_client_secret, get_db, app

''' OAuth '''

oauth_index = Blueprint('oauth_index', __name__)

@oauth_index.route('/oauth')
def zoho_oauth():
	db = get_db()
	cur_refresh_token = db.execute('SELECT zoho_refresh_token FROM tmp_oauth')
	refresh_token = cur_refresh_token.fetchone()[0]
	url = "https://accounts.zoho.com/oauth/v2/auth?scope=ZohoBooks.fullaccess.all&client_id={}&response_type=code&redirect_uri=http://127.0.0.1:5000/oauthcallback&access_type=offline".format(zoho_books_client_id)
	return render_template('oauth.html', zoho_url=url, refresh_token=refresh_token)

@oauth_index.route('/oauthcallback')
def zoho_oauth_callback():
	code = request.args.get('code')
	response = requests.post(
		'https://accounts.zoho.com/oauth/v2/token', params = {
		'code': code,
		'client_id': zoho_books_client_id,
		'client_secret': zoho_books_client_secret,
		'redirect_uri': "http://127.0.0.1:5000/oauthcallback",
		'grant_type': "authorization_code"
		})
	data = response.json()
	access_token = data['access_token']
	refresh_token = data['refresh_token']
	db = get_db()
	db.execute('INSERT OR REPLACE INTO tmp_oauth (zoho_refresh_token) VALUES (?)', [refresh_token])
	db.commit()
	auth_token = refresh_zoho_auth_token()
	return render_template('oauth.html', access_token=access_token, refresh_token=refresh_token, auth_token=auth_token)

def refresh_zoho_auth_token():
	db = get_db()
	cur_refresh_token = db.execute('SELECT zoho_refresh_token FROM tmp_oauth')
	refresh_token = cur_refresh_token.fetchone()[0]
	response = requests.post(
	'https://accounts.zoho.com/oauth/v2/token', params = {
	'refresh_token': refresh_token,
	'client_id': zoho_books_client_id,
	'client_secret': zoho_books_client_secret,
	'redirect_uri': "http://127.0.0.1:5000/oauthcallback",
	'grant_type': "refresh_token"
	})
	data = response.json()
	auth_token = data['access_token']
	db.execute('UPDATE tmp_oauth SET zoho_auth_token=? WHERE zoho_refresh_token=?', [auth_token, refresh_token])
	db.commit()
	return auth_token

def get_zoho_auth_token():
	db = get_db()
	cur_auth_token = db.execute('SELECT zoho_auth_token FROM tmp_oauth')
	auth_token = cur_auth_token.fetchone()[0]
	if auth_token:
		return auth_token
	else:
		flash("Please authenticate with Zoho Books.", "neutral")
		abort(404)