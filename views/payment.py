from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
import razorpay
from mangopay_globals import get_db, rz_key, razorpay_client

''' Payment Processing '''

payment = Blueprint('payment', __name__)

@payment.route('/make-payment/charge', methods=['POST'])
def charge():
    amount = request.form['payment']
    payment_id = request.form['razorpay_payment_id']
    parent_uid = request.form['parent_uid']
    course_uid = request.form['course_uid']
    razorpay_client.set_app_details({"title": "Mango Pay", "version": "1.2.0"})
    try:
    	razorpay_client.payment.capture(payment_id, amount)
    	response = razorpay_client.payment.fetch(payment_id)
    	
    	if response['captured'] == True and response['status'] == 'captured':
    		db = get_db()
    		db.execute('UPDATE tmp_parent_registration SET online_payment_status=?, payment_id=? \
    			WHERE parent_uid=?', ['Paid', payment_id, parent_uid])
    		db.commit()
    	else:
    		db = get_db()
    		db.execute('UPDATE tmp_parent_registration SET online_payment_status=?, payment_id=? \
    			WHERE parent_uid=?', ['Not Captured', payment_id, parent_uid])
    		db.commit()
    		response = "Payment not captured for {}. Please manually capture the amount.".format(payment_id) #TODO Mail?
    	flash("Payment Successful! You may close this tab now. Or make a new registration.", "success")
        return redirect(url_for('confirmation.confirm', course_uid=course_uid))

    except razorpay.errors.BadRequestError as e:
        flash(e, "error")
    	flash("Error when requesting payment confirmation.", "error")
    	return redirect(url_for('confirmation.confirm', course_uid=course_uid))