from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint, after_this_request
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db, rz_key, KalkinatorBot_key, telegram_group_id, send_mail, send_sms, celery

''' Registration and Payment '''

registration = Blueprint('registration', __name__)

@registration.route('/payment/', methods=['POST'])
def register():
	# VARs BEGIN
	course_uid = request.form['course_uid']
	parent_uid = str(request.form['parent_uid'])
	parent_name = request.form['parent_name']
	parent_email = request.form['parent_email']
	parent_contact = request.form['parent_contact']
	kid_name = request.form['kid_name']
	mode_of_payment = request.form['payment_choice']
	if mode_of_payment == "camp_total_online_fees":
		mode_of_payment = "online (full camp fee)"
	event_id = request.form['location']

	db = get_db()
	cur_course_details = db.execute('SELECT online_fees, course_id, \
		title, camp_total_online_fees \
		FROM tmp_course WHERE course_uid=?', [course_uid])
	course_details = cur_course_details.fetchone()
	if mode_of_payment == 'online':
		payment = course_details[0]*100
	elif mode_of_payment == 'online (full camp fee)':
		payment = course_details[3]*100
	course_id = course_details[1]
	course_name = course_details[2]
	# VARs END
	try:
		''' This registers the parents information. '''
		db.execute('INSERT INTO tmp_parent_registration \
			(parent_uid, parent_name, parent_email, parent_contact, kid_name, \
			course_id, mode_of_payment, event_id) \
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)', [parent_uid, 
			parent_name, parent_email, parent_contact, kid_name, 
			course_id, mode_of_payment, event_id])
		db.commit()
		''' Saving contact info in session to use in API calls at confirmation page '''
		session['parent_name'] = parent_name
		session['parent_email'] = parent_email
		session['parent_contact'] = parent_contact

		''' Once commited, the following methods sends EMAIL, SMS, and Telegram confirmations. '''
		message = "Hello {},\n\nYour registration for the program - {} is successful.\
		\n\nThank you,\nMango Education\n9952243541".format(parent_name, course_name)

		registration_url = "https://payments.themango.co"+url_for('registration_index.registrations_single', course_uid=course_uid, course_name=course_name)
		t_message = 'New registration for {}:\n\n<strong>Name:</strong> {}\n<strong>Number:</strong> {}\n<strong>Email:</strong> {}\n<strong>Kid Name:</strong> {}\n<strong>Payment:</strong> {}\n\nCheck <a href="{}">here</a> for more details.'.format(
			course_name, parent_name, parent_contact, parent_email, kid_name, mode_of_payment, registration_url)
		#send_mail(parent_email, parent_name, course_name, message)
		#send_sms(parent_contact, parent_name, course_name, message)
		kalki_url = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}&parse_mode=html".format(
			KalkinatorBot_key, telegram_group_id, t_message)
		send_telegram.delay(kalki_url) # Celery Task

	except sqlite3.IntegrityError:
		''' This checks if the parent is already registered.
			If their mode of payment is online and still haven't paid,
			they will be offered the payment page again.
		'''
		try:
			cur_payment_status = db.execute('SELECT online_payment_status, payment_id \
				FROM tmp_parent_registration WHERE parent_email = ? \
				AND course_id = ? AND parent_uid = ?', [parent_email, 
				course_id, parent_uid])
			payment_info = cur_payment_status.fetchall()[0]
			payment_status = payment_info[0]
			payment_id = payment_info[1]
		except IndexError:
			flash("Your registration exists already. Refresh and try again. If you \
			continue to face issues try a different email or contact us.", "error")
			return redirect(url_for('index.app_create', course_uid=course_uid))
		if mode_of_payment == "online" or mode_of_payment == "online (full camp fee)" and payment_status == None and payment_id == None:
			db = get_db()
			db.execute('UPDATE tmp_parent_registration SET mode_of_payment=? \
				WHERE parent_uid=?', [mode_of_payment, str(parent_uid)])
			db.commit()
			return render_template('make-payment.html', 
				parent_name=parent_name, parent_uid=parent_uid, parent_email=parent_email, 
				parent_contact=parent_contact, payment=payment, rz_key=rz_key, course_uid=course_uid)
		elif mode_of_payment == "offline":
			flash("Your registration already exists. \
			In case if you have cancelled your choice of online payment and have opted to pay offline, \
			we have updated it now as well. You can close this tab now. If you would like to make another registration, \
			click 'New Registration' button below.", "neutral")
			db = get_db()
			db.execute('UPDATE tmp_parent_registration SET mode_of_payment=?, \
				online_payment_status=?, payment_id=?\
				WHERE parent_uid=?', [mode_of_payment, "N/A", "N/A", str(parent_uid)])
			db.commit()
			return redirect(url_for('confirmation.confirm', course_uid=course_uid))
		else:
			flash("Your record already exists!", "error")
			return render_template("confirmation.html")

	if mode_of_payment == "offline" or mode_of_payment == "free":
		db = get_db()
		db.execute('UPDATE tmp_parent_registration SET online_payment_status=?, payment_id=? \
			WHERE parent_uid=?', ['N/A', 'N/A', str(parent_uid)])
		db.commit()
		if mode_of_payment == "offline": 
			flash("Your registration is successful. \
				You have chosen to pay the fees offline. You can close this tab now. \
				If you would like to make another registration, click 'New Registration' button below.", "success")
		else:
			flash("Your registration is successful. You can close this tab now. \
				If you would like to make another registration, click 'New Registration' button below.", "success")
		return redirect(url_for('confirmation.confirm', course_uid=course_uid))

	else:
		return render_template('make-payment.html', parent_name=parent_name, parent_uid=parent_uid, 
			parent_email=parent_email, parent_contact=parent_contact, payment=payment, rz_key=rz_key, course_uid=course_uid)

@celery.task
def send_telegram(kalki_url):
	try:
		requests.get(kalki_url)
	except:
		pass