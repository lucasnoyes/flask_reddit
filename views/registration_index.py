from flask import Flask, render_template, redirect, request, flash, g, url_for, session, abort, Blueprint
import uuid
import sqlite3
import requests
import datetime
from mangopay_globals import get_db

''' Registration - Index and single'''

registration_index = Blueprint('registration_index', __name__)

@registration_index.route('/registrations', methods=['GET'])
def registrations():
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_parents = db.execute('SELECT tmp_parent_registration.*, \
			tmp_course.title, tmp_location.location, tmp_event.date FROM tmp_parent_registration \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_parent_registration.course_id \
			INNER JOIN tmp_location ON tmp_location.location_id = tmp_event.location_id \
			INNER JOIN tmp_event ON tmp_event.event_id = tmp_parent_registration.event_id')
			parents = cur_parents.fetchall()
			return render_template('registration-index.html', parents=parents)
	else:
		return render_template('login.html')

@registration_index.route('/registrations/<course_uid>/<course_name>', methods=['GET'])
def registrations_single(course_uid, course_name):
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_course_id = db.execute('SELECT course_id FROM tmp_course WHERE course_uid=?', [course_uid])
			course_id = cur_course_id.fetchone()[0]
			cur_parents = db.execute('SELECT tmp_parent_registration.*, tmp_course.title, \
			tmp_location.location FROM tmp_parent_registration \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_parent_registration.course_id \
			INNER JOIN tmp_location ON tmp_location.location_id = tmp_event.location_id \
			INNER JOIN tmp_event ON tmp_event.event_id = tmp_parent_registration.event_id \
			WHERE tmp_parent_registration.course_id = ?', [course_id])
			parents = cur_parents.fetchall()

			return render_template('registration-index-single.html', parents=parents, course_name=course_name)
	else:
		return render_template('login.html')

@registration_index.route('/registrations/<course_uid>/<event_id>/<course_name>', methods=['GET'])
def registrations_single_event(course_uid, event_id, course_name):
	if 'username' in session:
		db = get_db()
		if request.method == 'GET':
			cur_course_id = db.execute('SELECT course_id FROM tmp_course WHERE course_uid=?', [course_uid])
			course_id = cur_course_id.fetchone()[0]
			cur_parents = db.execute('SELECT tmp_parent_registration.*, tmp_course.title, \
			tmp_location.location, tmp_event.date FROM tmp_parent_registration \
			INNER JOIN tmp_course ON tmp_course.course_id = tmp_parent_registration.course_id \
			INNER JOIN tmp_location ON tmp_location.location_id = tmp_event.location_id \
			INNER JOIN tmp_event ON tmp_event.event_id = tmp_parent_registration.event_id \
			WHERE tmp_parent_registration.course_id = ? AND tmp_event.event_id = ?', [course_id, event_id])
			parents = cur_parents.fetchall()
			event_date_cur = db.execute('SELECT date FROM tmp_event WHERE event_id=?', [event_id])
			event_date = event_date_cur.fetchone()[0]
			return render_template('registration-index-single-event.html', parents=parents, course_name=course_name, event_date=event_date)
	else:
		return render_template('login.html')